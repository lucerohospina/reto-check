# RetoCheck

### 1) Haz que el siguiente código funcione(usando js):

      [1,2,3,4,5].duplicator(); 
      // [1,2,3,4,5,1,2,3,4,5]

    let numbers = [1, 2, 3, 4, 5]

    function duplicate(myArray) {
      return myArray.concat(myArray)
     }

### 2) Implementar una función que devuelva el n número Fibonacci en un lenguaje de tu elección: 

        function fibonacci(n){
          return n < 1 ? 0 
          : n<=2 ? 1 
          : fibonacci(n-1) + fibonacci(n-2)
        }