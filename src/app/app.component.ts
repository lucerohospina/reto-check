import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  data = [
    { subject: "Matemáticas", subjectImage: "math.png", progress: 25, empty: "0", bronze: "1", silver: "5", golden: "5", diamond: "5" },
    { subject: "Física Básica", subjectImage: "physics.png", progress: 50, empty: "0", bronze: "1", silver: "5", golden: "5", diamond: "5" },
    { subject: "Biología General", subjectImage: "biology.png", progress: 75, empty: "0", bronze: "1", silver: "5", golden: "5", diamond: "5" }
  ]

}
